import React, { FC } from "react";
import { APIResult } from "./search-api";
import styles from "./search-result.module.scss";

type Props = {
  result: APIResult;
};

export const SearchResult: FC<Props> = ({ result }: Props) => {
  return (
    <div className={styles.searchResult}>
      <div
        className={styles.image}
        style={{
          backgroundImage: `url(${result.imageUrl})`,
        }}
      />
      <div className={styles.details}>
        <div className={styles.title}>{result.hotelName}</div>
        <div className={styles.rating}>
          {new Array(result.rating).fill(0).map(() => (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="currentColor"
            >
              <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
            </svg>
          ))}
        </div>
        <div className={styles.location}>Some Description</div>
      </div>
      <div className={styles.checkout}>
        <span className={styles.price}>€{result.price}</span>
        <span className={styles.totalText}>Total price per person</span>
        <button>View Details</button>
      </div>
    </div>
  );
};
