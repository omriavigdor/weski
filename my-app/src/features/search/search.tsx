import React, { useCallback, useEffect, useRef, useState } from "react";
import { APIResult, query } from "./search-api";
import { SearchResult } from "./search-result";
import styles from "./search.module.scss";
import { resorts } from "./resorts";

type Props = {};

export const Search = (props: Props) => {
  const [results, setResults] = useState<APIResult[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  const resortSelectionRef = useRef<HTMLSelectElement>(null);
  const numOfPeopleSelectionRef = useRef<HTMLSelectElement>(null);
  const startDateRef = useRef<HTMLInputElement>(null);
  const endDateRef = useRef<HTMLInputElement>(null);
  useEffect(() => {}, []);

  const search = useCallback(() => {
    setResults([]);
    setIsLoading(true);
    query(
      {
        skiSite: +(resortSelectionRef.current as HTMLSelectElement).value,
        fromDate: (startDateRef.current as HTMLInputElement).value,
        toDate: (endDateRef.current as HTMLInputElement).value,
        groupSize: +(numOfPeopleSelectionRef.current as HTMLSelectElement)
          .value,
      },
      (results) => setResults((r) => [...r, ...results]),
      () => {
        setIsLoading(false);
      }
    );
  }, []);

  return (
    <div>
      <div>
        <select ref={resortSelectionRef}>
          {resorts.map((resort) => (
            <option value={resort.id}>{resort.name}</option>
          ))}
        </select>
        <select ref={numOfPeopleSelectionRef}>
          {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((people) => (
            <option value={people.toString()}>{people} people</option>
          ))}
        </select>
        <input ref={startDateRef} type="date" />
        <input ref={endDateRef} type="date" />
        <button onClick={() => search()}>Search</button>
      </div>
      {isLoading && <h3>Loading Results...</h3>}
      <div className={styles.results}>
        {results
          .sort((x, y) => (x.price > y.price ? 1 : -1))
          .map((result) => (
            <SearchResult result={result} />
          ))}
      </div>
    </div>
  );
};
