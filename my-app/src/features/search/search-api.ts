export interface APIResult {
  hotelName: string;
  price: number;
  imageUrl?: string;
  rating: number;
}

export interface Query {
  skiSite: number;
  fromDate: string;
  toDate: string;
  groupSize: number;
}

const getQueryId = async (query: Query) => {
  const response = await fetch("/search", {
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(query),
  });
  return (await response.json()) as {
    queryId: string;
  };
};

export const query = async (
  query: Query,
  onResults: (results: APIResult[]) => void,
  onFinish: () => void
) => {
  let isFinished = false;
  const { queryId } = await getQueryId(query);
  const sendHttpRequest = async () => {
    const response = await fetch("/search/continue", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ queryId }),
    });
    return (await response.json()) as {
      results: APIResult[];
      finished: boolean;
    };
  };
  while (!isFinished) {
    const { finished, results } = await sendHttpRequest();
    onResults(results);
    isFinished = finished;
  }

  onFinish();
};
