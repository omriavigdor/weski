import React from "react";
import "./App.css";
import { Search } from "./features/search/search";

function App() {
  return (
    <div className="App">
      <Search />
    </div>
  );
}

export default App;
