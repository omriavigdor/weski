import express from "express";
import { mainRouter } from "./routes";

const PORT = 8080;

const start = async () => {
  const server = express();
  server.use(express.json());
  server.use(mainRouter());
  await new Promise<void>((res, rej) => {
    server.listen(PORT, res);
  });
};

start().then(() => {
  console.log("server started");
});
