import { Router } from "express";
import { WeSkiQuery } from "../apis/api";
import { findExistingSearch, searchAllApis } from "../services/search";

export const searchRouter = () => {
  const router = Router();
  router.post("/", async (req, res) => {
    const query = req.body as WeSkiQuery;
    const search = searchAllApis(query);
    res.json({ queryId: search.id });
  });
  router.post("/continue", async (req, res) => {
    const { queryId } = req.body as { queryId: string };
    const search = findExistingSearch(queryId);
    if (search.isFinished) {
      res.json({ results: search.results, finished: true });
      return;
    }
    search.onResult((results) => {
      res.json({ results, finished: search.isFinished });
    });
  });

  return router;
};
