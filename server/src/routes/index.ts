import { Router } from "express";
import { searchRouter } from "./search";

export const mainRouter = () => {
  const router = Router();

  router.get("/health", (req, res) => {
    res.json({ healthy: true });
  });

  router.use("/search", searchRouter());

  return router;
};
