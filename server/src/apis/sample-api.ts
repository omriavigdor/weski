import fetch from "node-fetch";
import { APIFactory } from "./api";

const URL =
  "https://gya7b1xubh.execute-api.eu-west-2.amazonaws.com/default/HotelsSimulator";

interface SampleAPIQuery {
  query: {
    ski_site: number;
    from_date: string;
    to_date: string;
    group_size: number;
  };
}

interface SampleAPIResult {
  body: {
    accommodations: {
      HotelName: string;
      PricesInfo: {
        AmountBeforeTax: string;
      };
      HotelDescriptiveContent: {
        Images: {
          MainImage: boolean;
          URL: string;
        }[];
      };
      HotelInfo: {
        Rating: string;
      };
    }[];
  };
}

export const sampleAPI: APIFactory<SampleAPIQuery, SampleAPIResult> = () => {
  const queryExternalAPI = async (body: any) => {
    const reponse = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });
    return reponse.json() as Promise<SampleAPIResult>;
  };

  return {
    query: (queries, onResult, onFinish) => {
      Promise.all(
        queries.map((query) =>
          queryExternalAPI(query).then((result) => onResult(result))
        )
      ).then(() => {
        onFinish();
      });
    },
    prepareQuery: (query) =>
      getRoomsBySmallestRoom(query.groupSize).map((roomSize) => ({
        query: {
          from_date: query.fromDate,
          group_size: roomSize,
          ski_site: query.skiSite,
          to_date: query.toDate,
        },
      })),
    prepareResult: (result) =>
      result.body.accommodations.map((accomodation) => ({
        hotelName: accomodation.HotelName,
        price: +accomodation.PricesInfo.AmountBeforeTax,
        imageUrl: accomodation.HotelDescriptiveContent.Images.find(
          ({ MainImage }) => MainImage
        )?.URL,
        rating: +accomodation.HotelInfo.Rating,
      })),
  };
};

const getRoomsBySmallestRoom = (smallestRoom: number) =>
  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].slice(smallestRoom);
