export interface WeSkiQuery {
  skiSite: number;
  fromDate: string;
  toDate: string;
  groupSize: number;
}

export interface WeSkiResult {
  hotelName: string;
  price: number;
  imageUrl?: string;
  rating: number;
}

/**
 * Any external api should implement this:
 */
export interface API<ApiQueryType, ApiResultType> {
  /**
   * query the api
   * yield results asynchrosnly with onResult
   * call onFinish when you are done
   */
  query: (
    query: ApiQueryType[],
    onResult: (result: ApiResultType) => void,
    onFinish: () => void
  ) => void;
  /**
   * convert weSki query (our app query) to one or more api queries
   */
  prepareQuery: (query: WeSkiQuery) => ApiQueryType[];
  /**
   * convert api Result to one or more weSkiResult
   */
  prepareResult: (query: ApiResultType) => WeSkiResult[];
}

export type APIFactory<Q, R> = () => API<Q, R>;
