import { API, WeSkiResult, WeSkiQuery } from "../apis/api";
import { sampleAPI } from "../apis/sample-api";
import { v4 as uuid } from "uuid";

const apis = [sampleAPI()];
const searches: Record<string, Search> = {};

const searchSingleAPI = async <Q, R>(
  api: API<Q, R>,
  query: WeSkiQuery,
  onResult: (result: WeSkiResult[]) => void,
  onFinish: () => void
) => {
  try {
    api.query(
      api.prepareQuery(query),
      (r) => onResult(api.prepareResult(r)),
      onFinish
    );
  } catch {
    // log error
    onResult([]);
    onFinish();
  }
};

type Search = {
  id: string;
  results: WeSkiResult[];
  onResult: (handler: (results: WeSkiResult[]) => void) => void;
  isFinished: boolean;
};

export const searchAllApis = (query: WeSkiQuery) => {
  let onResultHandler: Parameters<Search["onResult"]>[0] | undefined;
  const search: Search = {
    id: uuid(),
    isFinished: false,
    onResult: (handler) => {
      onResultHandler = handler;
      if (search.results.length > 0) {
        onResultHandler(search.results);
        onResultHandler = undefined;
        search.results.length = 0;
      }
    },
    results: [],
  };
  searches[search.id] = search;

  const onResult = (results: WeSkiResult[]) => {
    search.results.push(...results);
    if (onResultHandler) {
      onResultHandler(results);
      onResultHandler = undefined;
      search.results.length = 0;
    }
  };

  Promise.all(
    apis.map(
      (api) =>
        new Promise<void>((resolve) =>
          searchSingleAPI(api, query, onResult, resolve)
        )
    )
  ).then(() => {
    search.isFinished = true;
  });

  return search;
};

export const findExistingSearch = (id: string) => searches[id];
